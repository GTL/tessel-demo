var tessel = require('tessel');

//webserver was modified version from github https://github.com/dwhitnee/tessel
var http = require('http');      // the interwebs
var url = require('url');
var port = 8000;

//Get HW pointers
//var sdcard = require('sdcard').use(tessel.port['B']);
var gprs = require('gprs-sim900').use(tessel.port['D']);
var camera = require('camera-vc0706').use(tessel.port['A']);

var activeLED  = tessel.led[1]; // Blue LED, got a request
var readyLED   = tessel.led[0]; // Green LED, we are active
var comLED     = tessel.led[2];


var fs;
var sdReady     = true;
var gprsReady   = false;
var camReady    = false;
var tesselReady = false;

var ProcessQ = {
  tasks: [],
  Push: function(proc, inputs){
    ProcessQ.tasks.push({procedure: proc, args: inputs});
  },
  Pop: function(){
    return (ProcessQ.tasks.splice(0,1))[0];
  },
  Size: function(){
    return ProcessQ.task.length;
  },
  Execute: function(){
    if(ProcessQ.tasks.length > 0){
      var task = ProcessQ.Pop();
      task.procedure.apply(task.procedure, task.args);
    }
    
    
  }
}

var log = 'smslog.txt';
var baseFolder = 'images/'
//process.env.TESSEL_UPLOAD_DIR = '.';

//Turn off Comm LED (Yellow);
tessel.led[3].low();

//
//    MICROSD CARD SETUP
//
// sdcard.on('ready', function() {
  
//   console.log("SD Module is ready");
  
//   sdcard.getFilesystems(function(err, fss) {
//     sdReady = true;
//     console.log("MicroSD is Ready");
//     fs = fss[0];
//    if(tesselIsReady()){readyLED.high()};
//    });
// });


//
//  CAMERA SETUP
//
camera.on('ready', function() {
  camera.setResolution('vga', function(err) {
      if(err){
        console.log("Error setting resolution: " + err);
      }
      else{
        console.log("Camera is online");
        camReady = true;
        if(tesselIsReady()){readyLED.high();};
      }
  });
  
});
camera.on('error', function(err) {
  console.error('Camera Error: ' + err);
});


//
//  GPRS SETUP
//

gprs.on('ready', function() {
  console.log('GPRS module connected to Tessel. Searching for network...')
  //  Give it 10 more seconds to connect to the network, then try to send an SMS
  setTimeout(function() {
    gprsReady = true;
    if(tesselIsReady()){readyLED.high();};
    console.log('GPRS is now online');    
  }, 12000);
});

//  Handle errors
gprs.on('error', function (err) {
  console.log('Got an error of some kind:\n', err);
});

//  Emit unsolicited messages beginning with...
gprs.emitMe(['NORMAL POWER DOWN', 'RING', '+']);

gprs.on('NORMAL POWER DOWN', function powerDaemon () {
  gprs.emit('powered off');
  console.log('The GPRS Module is off now.');
});

gprs.on('RING', function someoneCalledUs () {
  console.log('Someone is trying to call the Tessel');
  CancelCall();
  
});

gprs.on('+', function handlePlus (data) {

  //See if its a text message
  var index = data.indexOf('"SM",');
  if(index > -1){
    console.log(data);
    console.log("Index: " + index);
    var id = data.substring(index + 5);
    console.log('Got a Text ID: ' + id);
    GetText(id, function(num, msg){
      console.log("Number:  " + num);
      console.log("Message: " + msg);
    });
  }
  else{
    console.log('Got an unsolicited message that begins with a \'+\'! Data:', data);
  }
  
});

//  Command the GPRS module via the command line
process.stdin.resume();
process.stdin.on('data', function (data) {
  data = String(data).replace(/[\r\n]*$/, '');  //  Removes the line endings
  console.log('got command', [data]);
  
  //Read Text
  if(data.indexOf("readtext") > -1){
    //readtext:id
    var command = data.split(':');
    var id  = command[1];
    console.log("Retrieving Text: " + id);
    GetText(id, function(number, message){
      console.log('Number:  ' + number);
      console.log('Message: ' + message);
    });
  }
  //Send a text
  else if(data.indexOf("sendtext") > -1){
    //sendtext:phonenumber:message
    var command = data.split(':');
    var number  = command[1];
    var msg     = command[2];
    
    console.log('Sending', msg, 'to', number, '...');
    SendText(number, msg);
  }
  //Delete a text
  else if(data.indexOf("deletetext") > -1){
    //sendtext:phonenumber:message
    var command = data.split(':');
    var id  = command[1];
    DeleteText(id, function(){
      console.log("Deleted Text: " + id);
    })
  }
  else if(data.indexOf("savelog") > -1){
    console.log("Saving Log to PC");
    SaveLog();
  }
  else if(data.indexOf("deletelog") > -1){
    console.log("Deleting Log");
    DeleteLog();
  }
  //forward to SIM900
  else{
    gprs._txrx(data, 10000, function(err, data) {
      console.log('\nreply:\nerr:\t', err, '\ndata:');
      data.forEach(function(d) {
        console.log('\t' + d);
      });
      console.log('');
    });
  } 
});

//
//  WEBSERVER SETUP
//

var Responses = {
  snapshot: function( response ) {
    //Take Picture and return filename
    //response.end(SnapShot());
    SnapShot("", function(image){
    	console.log("Image Returned");
    	response.end(image);
    });
  },
  sms: function(request, response){

    var query = request.url.split("/");
    console.log(query.join(','));
    var num = query[2];
    var msg = decodeURIComponent(query[3]);

    console.log("SMS request");

    SendText(num, msg, function() {

    	response.end(JSON.stringify({message:'Text Sent'}));	
      console.log("Sent Text\nNumber:  "+num+"\nMessage: "+msg);
    
    });
  }
};

var listener = function( request, response ) {

  console.log("Got a request to " + request.url );

  var req = url.parse( request.url, true );

  if ((request.url === "/snapshot")){

    Responses.snapshot( response );
  }
  else if(request.url.indexOf("/sms") > -1){
    Responses.sms( request, response );
  }
  else {
      response.end();
    } 
};

var server = http.createServer( listener ).listen( port );
console.log("\nServer running at http://127.0.0.1:" + port);
console.log("");

//
//  GPRS HELPER FUNCTIONS
//

var GetText = function(id, callback){
  if(tesselIsReady()){
    gprsReady = false;
    activeLED.high()
    gprs._txrx('at+cmgr='+id, 10000, function(err, data) {
        var number = (data[1].split(','))[1];
        var msg = data[2];
        DeleteText(id);
        //AppendLog("RCV, Number: " + number +", Message: " + msg);
        activeLED.low();
        if(callback){callback(number, msg);}
        gprsReady = true;
        ProcessQ.Execute();
      });
  }
  else{
    //Queue up task
    ProcessQ.Push(GetText, [id, callback]);
  }
}
var SendText = function(num, msg, callback){
  
  if(tesselIsReady()){
    gprsReady = false;
    activeLED.high();
    // Send message
    gprs.sendSMS(num, msg, function smsCallback(err, data) {
      if (err) {
        return console.log(err);
      }
      var success = data[0] !== -1;
      console.log('Text sent:', success);
      
      if (success) {
        // If successful, log the number of the sent text
        console.log('GPRS Module sent text #', data[0]);
        //AppendLog('SND, Number: "' + num +'", Message: ' + msg);
      }
      activeLED.low();
      gprsReady = true;
      if(callback){callback();}
      ProcessQ.Execute();
    });
  }
  else{
    ProcessQ.Push(SendText, [num, msg, calback]);
  }
}
var DeleteText = function(id, callback){
  if(tesselIsReady()){
    gprsReady = false;
    activeLED.high();
    gprs._txrx('at+cmgd='+id, 10000, function(err, data) {
      activeLED.low();
      if(callback){callback(id);};
      gprsReady = true;
      ProcessQ.Execute();
    });
  }
  else{
    ProcessQ.Push(DeleteText, [id, callback]);
  }
}
var CancelCall = function(){
  if(tesselIsReady()){
    gprsReady = false;
    activeLED.high();
    gprs._txrx('ATH', 10000, function(err, data) {
      activeLED.low();
      console.log('Declined Call');
      gprsReady = true;
      ProcessQ.Execute();
    });
  }
  else{
    ProcessQ.Push(CancelCall, []);
  }
}
//
//  MICRO SD HELPER FUNCTIONS
//

var DeleteLog = function(callback){
  if(sdReady){
    sdReady = false;
    fs.writeFile(log, "", function(){
      sdReady = true;
      if(callback){callback();}
      ProcessQ.Execute();
    });
  }
  else{
    ProcessQ.Push(DeleteLog, [callback])
  }
}

var GetLog = function(callback){

  if(sdReady){
    sdReady = false;
    fs.readFile(log, function(err, data) {
      sdReady = true;
      if(callback){callback(data.toString());}
      ProcessQ.Execute();
    });
  }
  else{
    ProcessQ.Push(GetLog, [callback]);
  }
}

var AppendLog = function(val, callback){
  if(sdReady){
    sdReady = false;
    fs.appendFile(log, val+'\n', function(){
      sdReady = true;
      if(callback){callback();}
      ProcessQ.Execute();
    })
  }
  else{
    ProcessQ.Push(AppendLog, [val, callback]);
  }
}

var SaveLog = function(callback){
  if(sdReady){
    sdReady = false;
    fs.readFile(log, function(err, data) {
      sdReady = true;
      process.sendfile(baseFolder + log, data);
      if(callback){callback(err, data);}
      ProcessQ.Execute();
    });
  }
  else{
    ProcessQ.Push(SaveLog, [callback]);
  }
}

//
//  CAMERA HELPER FUNCTIONS
//

//var filename;
var SnapShot = function(fname, callback){
  
  var time = Math.floor(Date.now());
  var filename = fname ? fname : 'image_' + time + '.jpg';
    
  if(tesselIsReady()){
    camReady = false;
    activeLED.high();
    // Take the picture
    camera.takePicture(function(err, image) {
        camReady = true;
        if (err) {
          console.log('error taking image', err);
        } 
        else {
          // Save the image
          callback(image);
          activeLED.low();
        }
        ProcessQ.Execute();
    }); 
  

  }
  else{
    ProcessQ.Push(SnapShot, [filename, callback]);
  }
  
  if(!fname){
    return 'images/' + filename;
  }
  
};

//
//  TESSEL FUNCTIONS
//

function tesselIsReady(){
  if(sdReady && gprsReady && camReady){
    return true;
  }
  else{
    return false;
  }
}

