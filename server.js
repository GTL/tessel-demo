     // the interwebs
var url  = require('url');
var fs = require('fs');
var express = require('express');
var app = express();
var http = require('http');
var server = http.Server(app);
//var http = require('http');
//var server = http.createServer(app); 
var io = require('socket.io')(server);

var port = 8080;
var tesselAPIServerIP = '192.168.1.3';
var tesselAPIServerPort = 8000;
var timeout = 45000;


var tessel ={
	ready: true,
	ProcessQ: {
	  tasks: [],
	  Push: function(proc, inputs){
	    tessel.ProcessQ.tasks.push({procedure: proc, args: inputs});
	  },
	  Pop: function(){
	    return (tessel.ProcessQ.tasks.splice(0,1))[0];
	  },
	  Size: function(){
	    return tessel.ProcessQ.task.length;
	  },
	  Execute: function(){
	    if(tessel.ProcessQ.tasks.length > 0){
	      var task = tessel.ProcessQ.Pop();
	      task.procedure.apply(task.procedure, task.args);
	    }
	  }
	},
	RequestSMS: function(number, message){
		if(tessel.isReady()){
			tessel.ready = false;
			var req = http.request({
			    port: tesselAPIServerPort,
			    method: 'GET',
			    hostname: tesselAPIServerIP,
			    path: '/sms/'+number+'/'+encodeURIComponent(message),
			    headers: {
			      Host: tesselAPIServerIP,
			      'Accept': '*/*',
			      "User-Agent": "tessel",
			      'Content-Type': 'text/html; charset=utf-8',
			      'Connection': 'keep-alive'
			    }
			}, function (res) {
			    res.on('data', function(tesselReply) {
			      //Responses.sms( response );
			      var data = JSON.parse(tesselReply);
			      console.log('    Tessel Replied: ' + data.message);
			      tessel.ready = true;
			      tessel.ProcessQ.Execute();
			    });
			});
			req.end();
			//console.log(req.path);
			req.on('socket', function (socket) {
			    socket.setTimeout(timeout);  
			    socket.on('timeout', function() {
			    	if(req){
			    		console.log("SMS request timed out " + message);
				        req.abort();
				        req = null;
				        setTimeout(function(){
				    		tessel.ready = true;
				    		tessel.ProcessQ.tasks.unshift({procedure: tessel.RequestSMS, args: [number, message]});
				    		tessel.ProcessQ.Execute();
				    	},1000);
				    }
			    });
			});
		    //Log any errors
		    req.on('error', function(e) {
		    	if(req){
		    		console.log("SMS Error: " + message);
			        req.abort();
			        req = null;
			        setTimeout(function(){
			    		tessel.ready = true;
			    		tessel.ProcessQ.tasks.unshift({procedure: tessel.RequestSMS, args: [number, message]});
			    		tessel.ProcessQ.Execute();
			    	},1000);
				}	
		    });

		  	console.log("    Requsting SMS");
		  	
		}
		else{
			tessel.ProcessQ.Push(tessel.RequestSMS, [number, message]);
		}
		
	},
	RequestPicture: function( response ){
		if(tessel.isReady()){
			tessel.ready = false;
			var req = http.request({
			    port: 8000,
			    method: 'GET',
			    hostname: tesselAPIServerIP,
			    path: '/snapshot',
			    headers: {
			      Host: tesselAPIServerIP,
			      'Accept': '*/*',
			      "User-Agent": "tessel",
			      'Content-Type': 'text/html; charset=utf-8',
			      'Connection': 'keep-alive'
			    }
			}, 
			function (res) {
			    var time = Math.floor(Date.now());
			    var filename = 'image_' + time + '.png';
			    var imagedata = ''
			    res.setEncoding('binary')
				  
			    res.on('data', function(chunk){
			        imagedata += chunk
				});
				  
				res.on('end', function(){
				    fs.writeFile('images/'+filename, imagedata, 'binary', function(err){
				            if (err) throw err
				            console.log('File saved.');
				        	tessel.ready = true;
				        	tessel.ProcessQ.Execute();
				            //Responses.snapshot(response, 'images/' + filename);
				    });     
			    });
		    });
		
			req.on('socket', function (socket) {
			    socket.setTimeout(timeout);  
			    socket.on('timeout', function() {
			    	if(req){
			    		console.log("Picture request timed out ");
				        req.abort();
				        req = null;
				        setTimeout(function(){
				    		tessel.ready = true;
				    		tessel.ProcessQ.tasks.unshift({procedure: tessel.RequestPicture, args: []});
				    		tessel.ProcessQ.Execute();
				    	},1000);
				    }

			    });
			});
			//Log any errors
		    req.on('error', function(e) {
		    	if(req){
		    		console.log("Error Requesting Picture:" + e);
			        req.abort();
			        req = null;
			        setTimeout(function(){
			    		tessel.ready = true;
			    		tessel.ProcessQ.tasks.unshift({procedure: tessel.RequestPicture, args: []});
			    		tessel.ProcessQ.Execute();
			    	},1000);
			    }

		    });

		  	console.log("    Requsting Picture");
		  	req.end();
		}
		else {
			tessel.ProcessQ.Push(tessel.RequestPicture, []);
		}
	},
	BasicRequest: function(number, message){
		var req = http.request({
			    port: tesselAPIServerPort,
			    method: 'GET',
			    hostname: tesselAPIServerIP,
			    path: '/sms/'+number+'/'+encodeURIComponent(message),
			    headers: {
			      Host: tesselAPIServerIP,
			      'Accept': '*/*',
			      "User-Agent": "tessel",
			      'Content-Type': 'text/html; charset=utf-8',
			      'Connection': 'keep-alive'
			    }
			}, function (res) {
				res.on('data', function(tesselReply) {
			      //Responses.sms( response );
			      var data = JSON.parse(tesselReply);
			      console.log('    Tessel Replied: ' + data.message);
			      tessel.ready = true;
			      tessel.ProcessQ.Execute();
			    });
			});
		req.end();
	},
	isReady: function(){
		return tessel.ready;
	}
}
//app.use(express.static(__dirname + '/'));
app.get('/', function(req, res){
  res.sendfile('index.html');
});
io.on('connection', function(socket){
	console.log("Got a connetion");
	socket.emit('chat message', "Hello Tessle");

  	socket.on('chat message', function(msg){
  		//console.log("Got chat message");
  		if(msg === "pic"){
  			console.log("Reqeusting Picture");
  			tessel.RequestPicture();
  		}
  		else{
  			console.log("Requesting Text");
  			tessel.RequestSMS('1234567890', msg);
  		}
  		
    //Sends to Everyone
    io.emit('chat message', msg);

    //Sends to everyone except sender
    //socket.broadcast.emit('hi');
  });
});



setTimeout(function(){
	//console.log("Sending Request");
 //tessel.RequestSMS('18179962396', 'Message from timeout');
 //tessel.BasicRequest('18179962396', 'From BasicRequest');

},1000);

server.listen(port);
console.log("Listening on port " + port);
